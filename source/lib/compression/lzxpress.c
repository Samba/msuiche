/*
 * Copyright (C) Matthieu Suiche 2008
 *
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the author nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "includes.h"
#include "replace.h"
#include "lzxpress.h"


#define __BUF_POS_CONST(buf,ofs)(((const uint8_t *)buf)+(ofs))
#define __PULL_BYTE(buf,ofs) \
  ((uint8_t)((*__BUF_POS_CONST(buf,ofs)) & 0xFF))

#ifndef PULL_UINT8
#define PULL_UINT8(buf,ofs) ((uint8_t)( \
				       ((uint8_t)(((uint8_t)(__PULL_BYTE(buf,(ofs)+0))) << 0)) \
					))
#endif


#ifndef PULL_LE_UINT16
#define PULL_LE_UINT16(buf,ofs) ((uint16_t)( \
					    ((uint16_t)(((uint16_t)(__PULL_BYTE(buf,(ofs)+0))) << 0)) | \
					    ((uint16_t)(((uint16_t)(__PULL_BYTE(buf,(ofs)+1))) << 8)) \
					     ))
#endif

#ifndef PULL_LE_UINT32
#define PULL_LE_UINT32(buf,ofs) ((uint32_t)( \
		((uint32_t)(((uint32_t)(__PULL_BYTE(buf,(ofs)+0))) <<  0)) | \
		((uint32_t)(((uint32_t)(__PULL_BYTE(buf,(ofs)+1))) <<  8)) | \
		((uint32_t)(((uint32_t)(__PULL_BYTE(buf,(ofs)+2))) << 16)) | \
		((uint32_t)(((uint32_t)(__PULL_BYTE(buf,(ofs)+3))) << 24)) \
))
#endif


#define PULL_INT8(buf,ofs)((int8_t)PULL_UINT8(buf,ofs))
#define PULL_LE_INT16(buf,ofs)((int16_t)PULL_LE_UINT16(buf,ofs))
#define PULL_LE_INT32(buf,ofs)((int32_t)PULL_LE_UINT32(buf,ofs))

static uint32_t xpress_write_metadata(uint32_t size, 
				      uint32_t offset, 
				      uint8_t *output, 
				      uint32_t pos, 
				      uint32_t nibble)
{
	uint32_t written_bytes;
	uint16_t metadata;
	uint32_t len;
	uint16_t *output2;

	output2 = (uint16_t *)&output[pos];
	len = size;
	len -= 3;
	offset--;

	if (len < 7) {
	  /* Classical meta-data */	 
		metadata = (uint16_t)((offset << 3) | len);
		output2[0] = metadata;
		written_bytes = sizeof(uint16_t);
	} else {
		metadata = (uint16_t)((offset << 3) | 7);
		output2[0] = metadata;
		written_bytes = sizeof(uint16_t);

		if ((len + 3) < (15 + 7 + 3)) {
			/* Shared byte */
	       		if (!nibble) {
				output[pos + written_bytes] |= (uint8_t)((len - 7) & 0xFF);
				written_bytes += sizeof(uint8_t);
			} else {
				output[nibble] |= (uint8_t)((len - 7) & 0xFF) << 4;
			}
		} else if ((len + 3) < (3 + 7 + 15 + 255)) {
		  /* Shared byte */
			if (!nibble) {
				output[pos + written_bytes] |= 15;
				written_bytes += sizeof(uint8_t);
			} else {
				output[nibble] |= 15 << 4;
			}
		   
			/* Additionnal len */
			if (!nibble) {
				output[pos + written_bytes] = (uint8_t)((len - (7 + 15)) & 0xFF);
			} else {
				output[pos + written_bytes] = (uint8_t)((len - (7 + 15)) & 0xFF);
			}

			written_bytes += sizeof(uint8_t);

		} else {		
			/* Shared byte */
			if (!nibble) {
				output[pos + written_bytes] |= 15;
				written_bytes += sizeof(uint8_t);
			} else {
				output[nibble] |= 15 << 4;
			}
		   
			/* Additionnal len */
			if (!nibble) {
				output[pos + written_bytes] = 255;
			} else {
				output[pos + written_bytes] = 255;
			}

			written_bytes += sizeof(uint8_t);

			output2[2] = (uint16_t)(len & 0xFFFF);
			written_bytes += sizeof(uint16_t);
		}

	}
	return written_bytes;
}

/*++
Function Name: xpress_write_indicator

Overview:

Parameters:
		-

Return Values:
		-
--*/
static void xpress_write_indicator(uint8_t **offset, 
				uint32_t *indicator, 
				uint8_t *output, 
				uint32_t *position)
{
	*(uint32_t *)(*offset) = *indicator;
	*indicator = 0;
	*offset = &output[*position];
	*position += sizeof(uint32_t);
}

/*
  Exchange team decide to compress identitical string with a minimum of 3 bytes matches.
  Moreover, if a series of identical bytes is present, it detects it correctly.
*/
static int xpress_compress(const uint8_t *in, 
			uint8_t *out, 
			uint32_t insize)
{
	uint32_t input_pos, output_pos, bytes_left;
	uint32_t max_offset, best_offset;
	int32_t offset;
	uint32_t max_length, length, best_length;
	uint8_t *string1, *string2;
	uint32_t indicator;
	uint8_t *indicator_pos;
	uint32_t indicator_bit, nibble_index;
	uint32_t metadata_size;

	if (!insize) return false;

	input_pos = 0;
	indicator = 0;
	output_pos = sizeof(uint32_t);
	indicator_pos = &out[0];

	bytes_left = insize;
	indicator_bit = 0;
	nibble_index = 0;

	do {
		if (input_pos > XPRESS_BLOCK_SIZE) {
			max_offset = XPRESS_BLOCK_SIZE;
		} else {
			max_offset = input_pos;
		}

		string1 = &in[input_pos];

		best_length = 2;
		best_offset = 0;

		for (offset = 1; offset <= max_offset; ++offset) {
			string2 = &string1[-offset];

			if ((string1[0] == string2[0]) &&
				(string1[best_length] == string2[best_length])) {
				max_length = (bytes_left < offset) ? bytes_left : offset;
				if (offset == 1) {
					if ((string1[0] == string1[1]) && (string1[0] == string1[2])) {
						for (length = 0; (length < bytes_left) && (string1[0] == string1[length]); ++length);

						if (length > best_length) {
							best_length = length;
							best_offset = 1;
						}
					}
				}

				for (length = 0; (length < max_length) && (string1[length] == string2[length]); ++length);

				if (length > best_length) {
					best_length = length;
					best_offset = offset;
				}
			}
		 }

		if ((best_length >= 3) && (best_offset <= 0x1FFF) && (best_length < bytes_left)) {
			metadata_size = xpress_write_metadata(best_length, best_offset, out, output_pos, nibble_index);
			
			indicator |= 1 << (32 - ((indicator_bit % 32) + 1));

			if (best_length > 10) {
				if (nibble_index == 0) {
					nibble_index = output_pos + sizeof(uint16_t);
				} else {
					nibble_index = 0;
				}
			}

			output_pos += metadata_size;

			input_pos += best_length;
			bytes_left -= best_length;
		} else {
			out[output_pos++] = in[input_pos++];
			bytes_left--;
		}

		indicator_bit++;

		if (((indicator_bit - 1) % 32) > (indicator_bit % 32)) {
			xpress_write_indicator(&indicator_pos,
						&indicator,
						out,
						&output_pos);
		}
	} while (bytes_left > 3);

	do {
		out[output_pos++] = in[input_pos];
		indicator_bit++;
		input_pos++;
	} while (input_pos < insize);

	if ((indicator_bit % 32) > 0) {
		for (indicator_bit; (indicator_bit % 32) != 0; indicator_bit++) {
			indicator |= 1 << (32 - ((indicator_bit % 32) + 1));
		}
		xpress_write_indicator(&indicator_pos,
					&indicator,
					out,
					&output_pos);
	}

	return output_pos;
}

uint32_t lzxpress_compress(const uint8_t *in_buf, 
			uint32_t in_len, 
			uint8_t *out_buf, 
			uint32_t out_len)
{
	return xpress_compress(in_buf, out_buf, out_len);
}

static uint32_t xpress_decompress(uint8_t *input, 
				uint32_t input_size, 
				uint8_t *output, 
				uint32_t output_size)
{
	uint32_t output_index, input_index;
	uint32_t indicator, indicator_bit;
	uint32_t length;
	uint32_t offset;
	uint32_t nibble_index;

	output_index = 0; 
	input_index = 0; 
	indicator = 0; 
	indicator_bit = 0; 
	length = 0; 
	offset = 0;
	nibble_index = 0; 

	do {
		if (indicator_bit == 0) {
			indicator = PULL_LE_UINT32(input, input_index);
			input_index += sizeof(uint32_t);
			indicator_bit = 32; 
		}
		indicator_bit--;

	/* check whether the bit specified by indicator_bit is set or not 
	   set in indicator. For example, if indicator_bit has value 4 
	   check whether the 4th bit of the value in indicator is set 
	*/
		if (((indicator >> indicator_bit) & 1) == 0) {
			output[output_index] = input[input_index]; 
			input_index += sizeof(uint8_t);
			output_index += sizeof(uint8_t);
		} else {
			length = PULL_LE_UINT16(input, input_index);
			input_index += sizeof(uint16_t); 
			offset = length / 8;
			length = length % 8;

			if (length == 7) {
				if (nibble_index == 0) {
					nibble_index = input_index;
					length = input[input_index] % 16; 
					input_index += sizeof(uint8_t);
				} else {
					length = input[nibble_index] / 16;
					nibble_index = 0;
				}

				if (length == 15) {
					length = input[input_index]; 
					input_index += sizeof(uint8_t); 
						if (length == 255) {
							length = PULL_LE_UINT16(input, input_index);
							input_index += sizeof(uint16_t);
							length -= (15 + 7); 
						}
					length += 15; 
				}
				length += 7; 
			}

			length += 3;

			do {
			  if (output_index >= output_size) break;
				output[output_index] = output[output_index - offset - 1];
				output_index += sizeof(uint8_t);
				length -= sizeof(uint8_t);
			} while (length != 0);
		}

	} while ((output_index < output_size) && (input_index < input_size));

	return output_index;
}

uint32_t lzxpress_decompress(DATA_BLOB *inbuf, 
			DATA_BLOB *outbuf)
{
	outbuf->length = xpress_decompress(inbuf->data, inbuf->length, outbuf->data, outbuf->length);
	return outbuf->length;
}
